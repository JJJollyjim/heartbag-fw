#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := heartbag

# COMPONENT_ADD_INCLUDEDIRS := main

EXTRA_CPPFLAGS := -DHAVE_CONFIG_H

EXTRA_CFLAGS := -Wno-error=unused-value -Wno-error=return-type -Wno-error=maybe-uninitialized

include $(IDF_PATH)/make/project.mk

