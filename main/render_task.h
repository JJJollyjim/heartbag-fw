#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"

TaskHandle_t render_task;

uint32_t frame_n;

uint16_t *lisp_pattern_buffer;
uint16_t *render_pattern_buffer;
uint16_t *pattern_a;
uint16_t *pattern_b;

void render_task_main(void *parameters);
