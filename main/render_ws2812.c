#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "i2s_driver.h"
#include "bag_config.h"
#include "esp_log.h"
#include "render_ws2812.h"

/* static uint32_t *i2s_bufs[2]; */
/* static int sending_buf=-1; */
/* static int calc_buf=0; */

const uint8_t _pin = 21;            // output pin number

size_t    _pixelsSize;    // Size of '_pixels' buffer 

uint32_t _i2sBufferSize; // total size of _i2sBuffer
uint8_t* _i2sBuffer;  // holds the DMA buffer that is referenced by _i2sBufDesc

const static int i2s_num = 0;
const static int i2s_sample_rate_div = 1;
const static int reset_time_us = 50;

const uint16_t c_dmaBytesPerPixelBytes = 4;
const uint16_t c_dmaBytesPer50us = 20;
const uint32_t c_dmaI2sSampleRate = 100000;

static bool ready_to_update()
{
    return (i2sWriteDone(i2s_num));
}

static void fill_buffer(uint16_t *ptn_buf)
{
    const uint16_t bitpatterns[16] =
        {
         0b1000100010001000, 0b1000100010001110, 0b1000100011101000, 0b1000100011101110,
         0b1000111010001000, 0b1000111010001110, 0b1000111011101000, 0b1000111011101110,
         0b1110100010001000, 0b1110100010001110, 0b1110100011101000, 0b1110100011101110,
         0b1110111010001000, 0b1110111010001110, 0b1110111011101000, 0b1110111011101110,
        };

    uint16_t* pDma = (uint16_t*)_i2sBuffer;
    uint16_t* pPixelsEnd = ptn_buf + _pixelsSize;
    for (uint16_t* pPixel = ptn_buf; pPixel < pPixelsEnd; pPixel += 3)
        {
            // G
            *(pDma++) = bitpatterns[(((*(pPixel + 1)) >> 8) & 0x0f)];
            *(pDma++) = bitpatterns[(((*(pPixel + 1)) >> 8) >> 4) & 0x0f];

            // R
            *(pDma++) = bitpatterns[(((*(pPixel + 0)) >> 8) & 0x0f)];
            *(pDma++) = bitpatterns[(((*(pPixel + 0)) >> 8) >> 4) & 0x0f];

            // B
            *(pDma++) = bitpatterns[(((*(pPixel + 2)) >> 8) & 0x0f)];
            *(pDma++) = bitpatterns[(((*(pPixel + 2)) >> 8) >> 4) & 0x0f];
        }
}

void render_ws2812_init() {
    /* //Allocate memory for the data buffers */
    /* for (int i=0; i<2; i++) { */
    /*     i2s_bufs[i]=heap_caps_malloc(DATA_BYTES, MALLOC_CAP_DMA); */
    /*     assert(i2s_bufs[i]!=NULL); */
    /*     memset(i2s_bufs[i], 0x00, DATA_BYTES); */
    /* } */
    const int elementSize = 3;

    uint16_t dmaPixelSize = c_dmaBytesPerPixelBytes * elementSize;
    uint16_t resetSize = (c_dmaBytesPer50us * reset_time_us / 50 / i2s_sample_rate_div);

    _pixelsSize = NUMLEDS * elementSize;
    _i2sBufferSize = NUMLEDS * dmaPixelSize + resetSize;

    // must have a 4 byte aligned buffer for i2s
    uint32_t alignment = _i2sBufferSize % 4;
    if (alignment) {
        _i2sBufferSize += 4 - alignment;
    }

    _i2sBuffer = (uint8_t*)heap_caps_malloc(_i2sBufferSize, MALLOC_CAP_DMA);
    memset(_i2sBuffer, 0x00, _i2sBufferSize);

    i2sInit(i2s_num, 16, c_dmaI2sSampleRate / i2s_sample_rate_div, I2S_CHAN_STEREO, I2S_FIFO_16BIT_DUAL, 2, 0);
    i2sSetPins(i2s_num, _pin, -1, -1, -1);
}


void render_ws2812_render_frame(uint16_t *ptn_buf) {

    while (!ready_to_update())
        {
            taskYIELD();
        }

    fill_buffer(ptn_buf);

    i2sWrite(i2s_num, _i2sBuffer, _i2sBufferSize, false, false);
}
