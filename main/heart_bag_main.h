/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * DEFINES
 ****************************************************************************************
 */

#define HRPS_HT_MEAS_MAX_LEN            (13)

#define HRPS_MANDATORY_MASK             (0x0F)
#define HRPS_BODY_SENSOR_LOC_MASK       (0x30)
#define HRPS_HR_CTNL_PT_MASK            (0xC0)


///Attributes State Machine
enum
{
    BAG_IDX_SVC,

    BAG_IDX_LIGHT_ON_CHAR,
    BAG_IDX_LIGHT_ON_VAL,

    BAG_IDX_PATTERN_CHAR,
    BAG_IDX_PATTERN_VAL,

    BAG_IDX_BRIGHTNESS_CHAR,
    BAG_IDX_BRIGHTNESS_VAL,

    BAG_IDX_STREAM_PTN_CHAR,
    BAG_IDX_STREAM_PTN_VAL,

    BAG_IDX_COLOUR_CHAR,
    BAG_IDX_COLOUR_VAL,

    BAG_IDX_NB,
};
