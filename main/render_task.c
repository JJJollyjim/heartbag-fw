#include <stdint.h>
#include <string.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "bag_config.h"
#include "render_lcd.h"
#include "render_ws2812.h"
#include "lisp_task.h"
#include "render_task.h"
#include "user_settings.h"

void apply_brightness(uint16_t *buf, uint16_t brightness) {
    for (int i = 0; i < NUMLEDS*3; i++) {
        buf[i] = (((uint32_t) buf[i]) * ((uint32_t) brightness)) >> 16;
    }
}

void render_task_main(void *parameters) {
    pattern_a = calloc(NUMLEDS*3, sizeof(uint16_t));
    assert(pattern_a != NULL);
    pattern_b = calloc(NUMLEDS*3, sizeof(uint16_t));
    assert(pattern_b != NULL);
    lisp_pattern_buffer = pattern_a;
    render_pattern_buffer = pattern_b;

    uint16_t *brightness = (uint16_t *) parameters;

    TickType_t xLastWakeTime = xTaskGetTickCount();;
    const TickType_t xFrequency = 2;

    frame_n = 0;

    /* render_lcd_init(); */
    render_ws2812_init();

    ESP_LOGI("RENDER", "starting renderer");

    while(1) {
        // Wait to be told the next pattern is ready
        uint32_t ignored;

        BaseType_t ret = xTaskNotifyWait(0, 0, &ignored, pdMS_TO_TICKS(100000));
        assert(ret == pdPASS);

        // Twiddle our thumbs until we want to draw the next frame
        vTaskDelayUntil( &xLastWakeTime, xFrequency );

        // Figure out which pattern we should now render, and which the lisp should next produce
        if (lisp_pattern_buffer == pattern_a) {
            render_pattern_buffer = pattern_a;
            lisp_pattern_buffer = pattern_b;
        } else {
            render_pattern_buffer = pattern_b;
            lisp_pattern_buffer = pattern_a;
        }

        frame_n++;

        // Tell lisp to start producing the next pattern
        xTaskNotify(lisp_task, 0, eNoAction);

        if (xQueuePeek(bag_light_on_q, &ignored, 0) == pdTRUE) {
            apply_brightness(render_pattern_buffer, *brightness);

            /* render_lcd_render_frame(render_pattern_buffer); */
            render_ws2812_render_frame(render_pattern_buffer);
        } else {
            // Display has just been turned off
            ESP_LOGI("RENDER", "turning off");

            memset(render_pattern_buffer, 0, sizeof(uint16_t) * NUMLEDS * 3);
            /* render_lcd_render_frame(render_pattern_buffer); */
            render_ws2812_render_frame(render_pattern_buffer);

            // Wait to turn back on
            xQueuePeek(bag_light_on_q, &ignored, portMAX_DELAY);

            xLastWakeTime = xTaskGetTickCount();
        }


        /* ESP_LOGI("RENDER", "frame"); */
        /* ESP_LOGI("RENDER", "RENDER's stack high water mark is %d\n", uxTaskGetStackHighWaterMark(NULL)); */
        /* ESP_LOGI("RENDER", "LISP's   stack high water mark is %d\n", uxTaskGetStackHighWaterMark(lisp_task)); */

        /* while(1) {} */
    }
}
