#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_partition.h"
#include "esp_log.h"

#include "leds.h"


void lisp_task_main(void *parameters) {
    ESP_LOGI("LISP", "starting interpreter");

    leds_loop();



    ESP_LOGE("LISP", "ending interpreter");
    while (1) {}
}
