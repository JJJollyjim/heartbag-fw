#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
QueueHandle_t  bag_light_on_q;
uint16_t       bag_pattern_val;
uint16_t       bag_brightness_val;
uint32_t       bag_colour_val;
