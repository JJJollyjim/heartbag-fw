struct RGB
{
	uint16_t R;
	uint16_t G;
	uint16_t B;
};

struct HSL
{
	float H;
	float S;
	float L;
};

struct RGB HSLToRGB(struct HSL hsl);
