#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "bag_config.h"
#include "rainbow.h"
#include "render_task.h"
#include "user_settings.h"

static const uint8_t ring_start[5] = {0, 34, 57, 68, 69}; // there isn't a 5th ring, but this allows us to not special-case the inner ring

uint16_t last_pattern_val = 0xFFFF;

static uint16_t x_position[NUMLEDS] = {265, 252, 239, 225, 212, 199, 185, 171, 155, 138, 120, 99, 84, 71, 67, 71, 81, 87, 81, 71, 67, 71, 83, 98, 118, 136, 155, 170, 185, 199, 212, 226, 239, 253, 232, 219, 205, 192, 179, 163, 147, 127, 110, 100, 107, 113, 113, 107, 101, 108, 125, 146, 162, 178, 192, 205, 219, 197, 184, 170, 155, 134, 137, 138, 134, 155, 171, 184, 162};
static uint16_t y_position[NUMLEDS] = {98, 110, 122, 134, 146, 158, 170, 181, 191, 199, 203, 200, 191, 175, 158, 138, 123, 103, 83, 68, 48, 31, 14, 4, 0, 1, 8, 17, 28, 40, 51, 63, 75, 86, 99, 111, 123, 135, 147, 158, 166, 169, 165, 147, 131, 114, 93, 76, 59, 41, 34, 34, 42, 52, 63, 75, 87, 100, 112, 124, 135, 135, 113, 87, 65, 65, 76, 88, 100};


void next_frame() {
    // "Hey, I finished that pattern you asked for"
    xTaskNotify(render_task, 0, eNoAction);

    // Wait to be told to get started on the next pattern
    uint32_t ignored;
    BaseType_t ret = xTaskNotifyWait(0, 0, &ignored, pdMS_TO_TICKS(100000));
    assert(ret == pdPASS);
}

void decay() {
    for (int i = 0; i < NUMLEDS*3; i++) {
        lisp_pattern_buffer[i] = render_pattern_buffer[i] * 5 / 6;
    }
}

float easeTime(float t, float b, float c, float d) {
    t /= d/2;
    if (t < 1) return c/2*t*t + b;
    t--;
    return -c/2 * (t*(t-2) - 1) + b;
}

void ptn_campfyre() {
    uint16_t user_r = (bag_colour_val >> 16) & 0xFF;
    uint16_t user_g = (bag_colour_val >>  8) & 0xFF;
    uint16_t user_b = (bag_colour_val >>  0) & 0xFF;

    decay();

    int idx = (frame_n % NUMLEDS) * 3;
    lisp_pattern_buffer[idx  ] = user_r << 8;
    lisp_pattern_buffer[idx+1] = user_g << 8;
    lisp_pattern_buffer[idx+2] = user_b << 8;
}

void ptn_heartsout() {
    int dur = 20;

    for (int ring = 0; ring < 4; ring++) {
        int time = (frame_n + (5*ring)) % (dur*2);

        uint16_t x;
        if (time < 20) {
            x =        (uint16_t) easeTime(time, 0, 0xFF, dur);
        } else {
            x = 0xFF - (uint16_t) easeTime(time-20, 0, 0xFF, dur);
        }

        for (int led = ring_start[ring]; led < ring_start[ring+1]; led++) {
            uint16_t user_r = (bag_colour_val >> 16) & 0xFF;
            uint16_t user_g = (bag_colour_val >>  8) & 0xFF;
            uint16_t user_b = (bag_colour_val >>  0) & 0xFF;

            lisp_pattern_buffer[led*3 + 0] = user_r * x;
            lisp_pattern_buffer[led*3 + 1] = user_g * x;
            lisp_pattern_buffer[led*3 + 2] = user_b * x;
        }
    }
}

void ptn_radialrainbow() {
    for (int i = 0; i < NUMLEDS - 1; i++) {
        int16_t dx = x_position[i] - x_position[NUMLEDS - 1];
        int16_t dy = y_position[i] - y_position[NUMLEDS - 1];

        float ang = atan2f((float) dy, (float) dx);

        /* if (dx <= 0) { */
        /*     ang += M_PI; */
        /* } */

        /* if (ang < 0) { */
        /*     ang += (M_PI * 2); */
        /* } */

        ang += frame_n * 0.05;

        ang = fmodf(ang, M_PI*2.0);

        const static float x1 = 1.26;
        const static float y1 = 0.6;

        if (ang < x1) {
            ang = (y1/x1)*ang;
        } else {
            ang = ((M_PI*2.0)-y1)/((M_PI*2.0)-x1) * ang + y1 - ((M_PI*2.0)-y1)/((M_PI*2.0)-x1) * x1;
        }

        struct HSL hsl = {ang, 1.0, 0.5};
        struct RGB rgb = HSLToRGB(hsl);
        lisp_pattern_buffer[i*3+0] = rgb.R;
        lisp_pattern_buffer[i*3+1] = rgb.G;
        lisp_pattern_buffer[i*3+2] = rgb.B;
    }
}

void ptn_ringspark() {
    int ring = 1;
    static int pos;

    if (frame_n == 0) {
        pos = ring_start[ring];
    }

    uint16_t user_r = (bag_colour_val >> 16) & 0xFF;
    uint16_t user_g = (bag_colour_val >>  8) & 0xFF;
    uint16_t user_b = (bag_colour_val >>  0) & 0xFF;

    decay();

    pos++;

    if (pos == ring_start[ring+1]) {
        pos = ring_start[ring];
    }

    lisp_pattern_buffer[pos*3+0] = user_r << 8;
    lisp_pattern_buffer[pos*3+1] = user_g << 8;
    lisp_pattern_buffer[pos*3+2] = user_b << 8;
}

void ptn_catalyst() {
  uint8_t lighting[NUMLEDS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,0,0,0,1,1,1,0,0,0,0,0,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1};
  for (int i = 0; i < NUMLEDS; i++) {
    if(lighting[i]){
      lisp_pattern_buffer[i*3] = 0xff00;
      lisp_pattern_buffer[i*3+1] = 0xdd00;
      lisp_pattern_buffer[i*3+2] = 0xdd00;
    } else {
      lisp_pattern_buffer[i*3] = 0xff00;
      lisp_pattern_buffer[i*3+1] = 0x0000;
      lisp_pattern_buffer[i*3+2] = 0x0000;
    }
  }
}

static void (*pattern_funcs[])() =
    {
     &ptn_campfyre,
     &ptn_heartsout,
     &ptn_radialrainbow,
     &ptn_ringspark,
     &ptn_catalyst
    };

void leds_loop() {
    while(1) {
        if (lisp_pattern_buffer != NULL) {
            if (bag_pattern_val != last_pattern_val) {
                memset(lisp_pattern_buffer, 0, sizeof(uint16_t) * NUMLEDS * 3);
                frame_n = 0;
                last_pattern_val = bag_pattern_val;
            }

            if (bag_pattern_val < sizeof(pattern_funcs)/sizeof(pattern_funcs[0])) {
                (*pattern_funcs[bag_pattern_val])();
            }
        }
        next_frame();
    }
}
